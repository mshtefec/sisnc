@extends('layouts.app')

@section('title', 'Registrar Producto')

@section('content')
    <br>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Registro de Productos</h3>
            <h4>
                <a href="{{ route('productos.index') }}">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                    Listar Productos
                </a>
            </h4>
        </div>
        <div class="panel-body">
            <form method="post" action="/productos">
                @include('producto.form')
                <button type="submit" class="btn btn-default">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Registrar
                </button>
            </form>
        </div>
    </div>
@endsection
