<input type="hidden" name="_token" value="{{ csrf_token() }}">

@if(isset($producto))
    <div class="form-group">
        <label for="#">Nombre</label>
        <input type="text" name="nombre" class="form-control" placeholder="Nombre" value="{{ $producto->nombre }}" required>
    </div>
@else
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <label for="#">Nombre</label>
                <input type="text" name="nombre" class="form-control" placeholder="nombre" required>
            </div>
        </div>
    </div>
@endif
