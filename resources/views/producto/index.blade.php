@extends('layouts.app')

@section('title', 'Listado Productos')

@section('content')
    <br>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Listado de Productos</h3>
            <h4>
                <a href="{{ route('productos.create') }}">
                    <span class="glyphicon glyphicon-plus"></span>
                    Registrar Producto
                </a>
            </h4>
        </div>
            <div class="table-responsive">
                @if($data)
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $row)
                            <tr>
                                <td>{{ $row->nombre }}</td>
                                <td>
                                    <a class="glyphicon glyphicon-search tooltips" style="color: black;" href="#" title="ver" rel="tooltip"></a>
                                    <a class="glyphicon glyphicon-edit tooltips" style="color: black;" href="{{ route('productos.edit', $row->id) }}" title="editar" rel="tooltip"></a>
                                    <a class="glyphicon glyphicon-remove tooltips" href="#" onclick="document.getElementById('delete').submit()" style="color: black;" title="borrar" rel="tooltip"></a>
                                    <form action="{{ route('productos.destroy', $row->id) }}" method="post" id="delete">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" style="display: none;"></button>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                @endif
            </div>
    </div>
@endsection
