<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    /**
     * Get the post that owns the comment.
     */
    public function categoria()
    {
        return $this->belongsTo('App\Categoria');
    }
}
