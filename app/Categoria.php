<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    /**
     * Get the comments for the blog post.
     */
    public function productos()
    {
        return $this->hasMany('App\Producto');
    }
}
